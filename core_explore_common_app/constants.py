"""
Constants for core explore common app
"""
from django.conf import settings

LOCAL_QUERY_NAME = "Local"
LOCAL_QUERY_URL = "core_explore_common_local_query"
LOCAL_QUERY_SERVER = getattr(settings, 'LOCAL_QUERY_SERVER', 'http://localhost:8080')